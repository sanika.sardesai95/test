from flask_pymongo import pymongo
import urllib.parse
import os

class MongoDB:
    def __init__(self):
        self.mongodb_username = os.getenv("MONGODB_USERNAME")
        self.mongodb_password = os.getenv("MONGODB_PASSWORD")
        self.cluster_url = os.getenv("CLUSTER_URL")
        print(self.cluster_url)

    def mongodb_connection(self):
        """
        This method establishes a connection to a MongoDB database

        :return: None
        """
        connection_string = "mongodb+srv://{}:{}@{}".format(
            urllib.parse.quote(self.mongodb_username),
            urllib.parse.quote(self.mongodb_password),
            urllib.parse.quote(self.cluster_url)
        )
        client = pymongo.MongoClient(connection_string)
        return client

    def save_data_mongodb(self, data):
        mongodb_client = self.mongodb_connection()
        mongodb_client.cbd3345_s24_s1.userinfo.insert_one(data)
